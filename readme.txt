How to run this project


1. Install go in your machine and add go in to class path

2. Generate WSO2 certificate from API manager and add it to your machine
( or else you can add it to your client application by writing a code as well)

3. Start WSO2 API manager

4. Go to API manager store and generate keys for your application

3. clone this project

4. Go to the source directory using a terminal

5. execute following command
go run .
this is similar as running all go files
in my case it is similar to
go run main.go login.go logout.go structs.go getMenu.go getRefreshToken.go home.go

6. Open a browser

7. go to http://localhost:3000/

8. Enter client Id and Client secret generates from WSO2 api manager

9. If you are redirected to WSO2 APIM login screen, give your WSO2 APIM credentials


You are done now

