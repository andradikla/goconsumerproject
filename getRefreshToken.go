package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func refresh() {

	var c Configurations
	file, err := os.Open("config/config.dev.json")
	if err != nil {
		fmt.Println("cannot open configs file")
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&c)
	if err != nil {
		fmt.Println("cannot decode configs file")
	}

	httpClient := http.Client{}

	form := url.Values{
		"client_id":     {clientID},
		"client_secret": {clientSecret},
		"refresh_token": {refreshToken},
		"grant_type":    {"refresh_token"},
		"redirect_uri":  {c.RedirectURL}}

	req, err := http.NewRequest(http.MethodPost, c.TokenURL, strings.NewReader(form.Encode()))
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", err)
		return
	}

	res, err := httpClient.Do(req)
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not send HTTP request: %v", err)
		return
	}

	var t OAuthAccessResponse
	if err := json.NewDecoder(res.Body).Decode(&t); err != nil {
		fmt.Fprintf(os.Stdout, "could not parse JSON response: %v", err)
		//w.WriteHeader(http.StatusBadRequest)
	}

	accessToken = t.AccessToken
	fmt.Print("access token : ")
	fmt.Println(accessToken)

	refreshToken = t.RefreshToken
	fmt.Print("refresh token : ")
	fmt.Println(refreshToken)

	fmt.Print("expires in : ")
	fmt.Println(t.ExpiresIn)

	loggedIn = true

	//w.Header().Set("Location", "/welcome.html")
	//w.WriteHeader(http.StatusFound)
}
