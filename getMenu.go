package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"text/template"
)

func listMenuItems(pizzaStr []byte) PizzaMenuData {

	var items []MenuItem
	error := json.Unmarshal(pizzaStr, &items)
	if error != nil {
		fmt.Println(error)
	}
	fmt.Println(items)

	Data := PizzaMenuData{
		Title:     "pizza menu",
		MenuItems: items,
	}
	return Data
}

func getMenu(w http.ResponseWriter, r *http.Request) {

	if loggedIn {

		httpClient1 := http.Client{}
		if loggedIn {
			fmt.Println("handling menu items")
			fmt.Println(accessToken)

			//get menu from pizza shack API
			req, err := http.NewRequest(http.MethodGet, "https://localhost:8243/pizzashack/1.0.0/menu", nil)
			if err != nil {
				fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", err)
				w.WriteHeader(http.StatusBadRequest)
			}
			req.Header.Set("accept", "application/json")
			req.Header.Set("Authorization", "Bearer "+accessToken)

			res1, err1 := httpClient1.Do(req)
			if err1 != nil {
				fmt.Fprintf(os.Stdout, "could not send HTTP request: %v", err1)
				return
			}
			if res1.StatusCode == 401 {
				fmt.Println("unauthorized access")

				var errRes ErrorResponse

				data1, _ := ioutil.ReadAll(res1.Body)
				err2 := json.Unmarshal(data1, &errRes)
				if err2 != nil {
					fmt.Println(err2)
					return
				}

				if errRes.FaultObj.Code == 900901 {
					refresh()
				}

				req.Header.Set("Authorization", "Bearer "+accessToken)

				res1, err1 = httpClient1.Do(req)
				if err1 != nil {
					fmt.Fprintf(os.Stdout, "could not send HTTP request: %v", err1)
					return
				}

				//w.WriteHeader(http.StatusInternalServerError)
			}

			data, _ := ioutil.ReadAll(res1.Body)
			fmt.Println(data)
			tmpl := template.Must(template.ParseFiles("html/menu.html"))
			PizzaData := listMenuItems(data)

			tmpl.Execute(w, PizzaData)
			w.Header().Set("Location", "/menu.html")
			//w.WriteHeader(http.StatusFound)
		} else {
			fmt.Println("user is not logged in. Please login")
			w.Header().Set("Location", "/index.html")
			w.WriteHeader(http.StatusFound)
		}
	} else {
		w.Header().Set("Location", "/login.html")
		w.WriteHeader(http.StatusFound)
	}

}
