package main

import (
	"net/http"
)

var clientID = ""
var clientSecret = ""
var accessToken = ""
var accessCode = ""
var refreshToken = ""

var loggedIn = false

func main() {

	http.HandleFunc("/", home)

	http.HandleFunc("/login", loginM)

	http.HandleFunc("/menu", getMenu)

	http.HandleFunc("/logout", logoutM)

	http.HandleFunc("/callback", callback)

	http.ListenAndServe(":3000", nil)
}
