package main

//Configurations has config details
type Configurations struct {
	TokenURL    string
	AuthURL     string
	RedirectURL string
}

//OAuthAccessResponse response oauth access token
type OAuthAccessResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
	ToeknType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
}

//ErrorResponse struct to handle error
type ErrorResponse struct {
	FaultObj Fault `json:"fault"`
}

//ClientInfo .....
type ClientInfo struct {
	CID         string
	LoginStatus bool
}

//Fault is struct for fault.json
type Fault struct {
	Code        int    `json:"code"`
	Message     string `json:"message"`
	Description string `json:"description"`
}

//MenuItem indicates pizza item
type MenuItem struct {
	Name        string
	Description string
	Price       string
	Icon        string
}

//PizzaMenuData for pizza menu
type PizzaMenuData struct {
	Title     string
	MenuItems []MenuItem
}

//LogInDetails to get client id, secret
type LogInDetails struct {
	ClientID     string
	ClientSecret string
}
