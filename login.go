package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"text/template"
)

func loginM(w http.ResponseWriter, r *http.Request) {

	if !loggedIn {

		fmt.Println("handle login")

		loginDetails := LogInDetails{
			ClientID:     r.FormValue("client_id"),
			ClientSecret: r.FormValue("client_secret"),
		}

		fmt.Print("login details : ")
		fmt.Println(loginDetails)

		clientID = loginDetails.ClientID
		clientSecret = loginDetails.ClientSecret

		var c Configurations
		file, err := os.Open("config/config.dev.json")
		if err != nil {
			fmt.Println("cannot open configs file")
		}
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&c)
		if err != nil {
			fmt.Println("cannot decode configs file")
		}

		fmt.Print("auth url : ")
		fmt.Println(c.AuthURL)
		fmt.Print("redirect url : ")
		fmt.Println(c.RedirectURL)
		fmt.Print("token url : ")
		fmt.Println(c.TokenURL)

		authReqURL := fmt.Sprintf("%s?client_id=%s&redirect_uri=%s&response_type=code", c.AuthURL, loginDetails.ClientID, c.RedirectURL)
		fmt.Println(authReqURL)

		// authReq, authErr := http.NewRequest(http.MethodGet, authReqURL, nil)
		// if authErr != nil {
		// 	fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", authErr)
		// 	w.WriteHeader(http.StatusBadRequest)
		// }

		// resq, _ := http.Get(authReqURL)
		// fmt.Println("get response  >>>>>>>>>> ")
		// fmt.Println(resq)
		w.Header().Set("Location", authReqURL)
		w.WriteHeader(http.StatusFound)
	}
}

func callback(w http.ResponseWriter, r *http.Request) {

	httpClient := http.Client{}

	fmt.Println("within callback")
	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not parse query: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	code := r.FormValue("code")
	fmt.Print("access code : ")
	fmt.Println(code)

	var c Configurations
	file, err := os.Open("config/config.dev.json")
	if err != nil {
		fmt.Println("cannot open configs file")
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&c)
	if err != nil {
		fmt.Println("cannot decode configs file")
	}

	form := url.Values{
		"client_id":     {clientID},
		"client_secret": {clientSecret},
		"code":          {code},
		"grant_type":    {"authorization_code"},
		"redirect_uri":  {c.RedirectURL}}

	req, err := http.NewRequest(http.MethodPost, c.TokenURL, strings.NewReader(form.Encode()))
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//req.Header.Set("accept", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not send HTTP request: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer res.Body.Close()

	var t OAuthAccessResponse
	if err := json.NewDecoder(res.Body).Decode(&t); err != nil {
		fmt.Fprintf(os.Stdout, "could not parse JSON response: %v", err)
		w.WriteHeader(http.StatusBadRequest)
	}

	accessToken = t.AccessToken
	fmt.Print("access token : ")
	fmt.Println(accessToken)

	refreshToken = t.RefreshToken
	fmt.Print("refresh token : ")
	fmt.Println(refreshToken)

	fmt.Print("expires in : ")
	fmt.Println(t.ExpiresIn)

	loggedIn = true

	data := ClientInfo{
		CID:         clientID,
		LoginStatus: loggedIn,
	}

	tmpl := template.Must(template.ParseFiles("html/welcome.html"))

	tmpl.Execute(w, data)
	w.Header().Set("Location", "/welcome")
	//	w.WriteHeader(http.StatusFound)

}
