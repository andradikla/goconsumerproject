package main

import (
	"fmt"
	"net/http"
	"text/template"
)

func home(w http.ResponseWriter, r *http.Request) {
	data := ClientInfo{
		CID:         clientID,
		LoginStatus: loggedIn,
	}

	if !loggedIn {
		fmt.Println("not logged in")
		// fs := http.FileServer(http.Dir("html"))
		// http.Handle("/", fs)
		tmpl := template.Must(template.ParseFiles("html/index.html"))
		tmpl.Execute(w, data)
		w.Header().Set("Location", "/home")

	} else {
		fmt.Println("logged in")
		data := ClientInfo{
			CID:         clientID,
			LoginStatus: loggedIn,
		}

		tmpl := template.Must(template.ParseFiles("html/welcome.html"))

		tmpl.Execute(w, data)
		w.Header().Set("Location", "/welcome")
	}
}
