package main

import (
	"fmt"
	"net/http"
)

func logoutM(w http.ResponseWriter, r *http.Request) {
	loggedIn = false
	accessToken = ""
	fmt.Println("user is not logged in. Please login")
	w.Header().Set("Location", "/index.html")
	w.WriteHeader(http.StatusFound)
}
